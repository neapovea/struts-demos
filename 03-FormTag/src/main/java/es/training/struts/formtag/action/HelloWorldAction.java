package es.training.struts.formtag.action;

import com.opensymphony.xwork2.ActionSupport;

import es.training.struts.formtag.model.MessageStore;

public class HelloWorldAction extends ActionSupport {

    private static final long serialVersionUID = 1L;

    private MessageStore messageStore;
    
    private static int helloCount = 0;
    
    public int getHelloCount() {
        return helloCount;
    }

    public String execute() {
        messageStore = new MessageStore() ;
        
        helloCount++;
        
        return SUCCESS;
    }

    public MessageStore getMessageStore() {
        return messageStore;
    }

}
