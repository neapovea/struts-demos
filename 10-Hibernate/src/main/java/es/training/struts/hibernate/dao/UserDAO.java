package es.training.struts.hibernate.dao;

import es.training.struts.hibernate.model.User;

public interface UserDAO {

	User getUserByCredentials(String userId, String password);
}
