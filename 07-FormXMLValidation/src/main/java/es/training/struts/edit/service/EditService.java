package es.training.struts.edit.service;

import es.training.struts.edit.model.Person;

public interface EditService {

    Person getPerson();

    void savePerson(Person personBean);

}
