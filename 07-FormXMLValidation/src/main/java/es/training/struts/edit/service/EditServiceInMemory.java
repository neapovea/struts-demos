package es.training.struts.edit.service;

import es.training.struts.edit.model.Person;

public class EditServiceInMemory implements EditService {

    private static Person person;
    private static String[] carModels = {"Ford", "Nissan"};

    static {

        person = new Person();
        person.setFirstName("Emilio");
        person.setLastName("Garcia");
        person.setEmail("emilio.garcia@struts.es");
        person.setSport("baloncesto");
        person.setGender("masculino");
        person.setResidency("ES");
        person.setOver21(true);
        person.setCarModels(carModels);
        person.setPhoneNumber("666-666-666");

    }

    public Person getPerson() {
        return EditServiceInMemory.person;
    }

    public void savePerson(Person personBean) {

        EditServiceInMemory.person.setFirstName(personBean.getFirstName());
        EditServiceInMemory.person.setLastName(personBean.getLastName());
        EditServiceInMemory.person.setSport(personBean.getSport());
        EditServiceInMemory.person.setGender(personBean.getGender());
        EditServiceInMemory.person.setResidency(personBean.getResidency());
        EditServiceInMemory.person.setOver21(personBean.isOver21());
        EditServiceInMemory.person.setCarModels(personBean.getCarModels());
        EditServiceInMemory.person.setEmail(personBean.getEmail());
        EditServiceInMemory.person.setPhoneNumber(personBean.getPhoneNumber());

    }

}
