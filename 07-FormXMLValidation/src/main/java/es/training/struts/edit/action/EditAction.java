package es.training.struts.edit.action;

import com.opensymphony.xwork2.ActionSupport;
import es.training.struts.edit.model.Person;
import es.training.struts.edit.model.State;
import es.training.struts.edit.service.EditService;
import es.training.struts.edit.service.EditServiceInMemory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class EditAction extends ActionSupport {

    private EditService editService = new EditServiceInMemory();

    private Person personBean;

    private String[] sports = {"futbol", "baloncesto", "atletismo"};

    private String[] genders = {"masculino", "femenino", "no seguro"};

    private List<State> states;

    private String[] carModelsAvailable = {"Ford", "Chrysler", "Toyota", "Nissan"};

    public String execute() throws Exception {
        editService.savePerson(getPersonBean());
        return SUCCESS;
    }

    public String input() throws Exception {
        setPersonBean(editService.getPerson());
        return INPUT;
    }

    public Person getPersonBean() {
        return personBean;
    }

    public void setPersonBean(Person person) {
        personBean = person;
    }

    public List<String> getSports() {
        return Arrays.asList(sports);
    }

    public List<String> getGenders() {
        return Arrays.asList(genders);
    }

    public List<State> getStates() {
        states = new ArrayList<State>();
        states.add(new State("ES", "España"));
        states.add(new State("FR", "Francia"));
        states.add(new State("IT", "Italia"));
        states.add(new State("PR", "Portugal"));

        return states;
    }

    public String[] getCarModelsAvailable() {
        return carModelsAvailable;
    }

}
