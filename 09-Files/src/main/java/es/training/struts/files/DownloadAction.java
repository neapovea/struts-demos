package es.training.struts.files;

import com.opensymphony.xwork2.ActionSupport;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;


public class DownloadAction extends ActionSupport {

	private static final long serialVersionUID = 3417532091177617332L;

	private InputStream fileInputStream;
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public String execute() throws Exception {
	    fileInputStream = new FileInputStream(new File("C://example.txt"));
	    return SUCCESS;
	}
	
	
}
