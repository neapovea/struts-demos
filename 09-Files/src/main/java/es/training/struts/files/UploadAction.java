package es.training.struts.files;

import com.opensymphony.xwork2.ActionSupport;

import java.io.File;

public class UploadAction extends ActionSupport {

	private static final long serialVersionUID = 3417532091177617332L;

	private File[] upload;
    private String[] uploadFileName;
    private String[] uploadContentType;

    public String execute() throws Exception {
        return INPUT;
    }

    public File[] getUpload() {
        return upload;
    }

    public void setUpload(File[] upload) {
        this.upload = upload;
    }

    public String[] getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String[] uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String[] getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(String[] uploadContentType) {
        this.uploadContentType = uploadContentType;
    }
}
