<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>IFCT034PO | 09-Files</title>
	</head>
	
	<body>
		<h1>¡Bienvenido a Struts 2!</h1>
		
		<h2>Descarga de fichero</h2>
		<div style="margin-left:50px">
			<p><a href="<s:url action='download'/>">Descargar fichero C://example.txt</a></p>		
		</div>
		
		<h2>Subida de fichero</h2>
		<div style="margin-left:50px">
		<s:form action="upload" method="post" enctype="multipart/form-data">
			<s:file name="upload" />
			<s:file name="upload" />
			<s:file name="upload" />
			<s:submit value="Subir ficheros" />
		</s:form>
		
		<p><b>Ficheros: </b><br/>
		<s:iterator value="upload" var="u">
		    <s:property value="u"/><br/>
		</s:iterator></p>
		
		<p><b>Tipos: </b><br/>
		<s:iterator value="uploadContentType" var="ct">
		    <s:property value="ct"/><br/>
		</s:iterator></p>
		
		<p><b>Nombre ficheros: </b><br/>
		<s:iterator value="uploadFileName" var="fn">
		    <s:property value="fn"/><br/>
		</s:iterator></p>
		</div>
	</body>
</html>


