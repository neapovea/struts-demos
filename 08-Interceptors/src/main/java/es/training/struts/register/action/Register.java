package es.training.struts.register.action;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import es.training.struts.register.model.Person;

public class Register extends ActionSupport implements ModelDriven<Person>, Preparable {

	private static final long serialVersionUID = 1L;

	private Person personBean;

	public Register() {
		personBean = new Person();
	}

	@Override
	public String execute() throws Exception {
		System.out.println("actual business logic");
		return SUCCESS;
	}

	@Override
	public Person getModel() {
		return personBean;
	}

	@Override
	public void prepare() throws Exception {
		System.out.println("preparation logic");

	}

}
