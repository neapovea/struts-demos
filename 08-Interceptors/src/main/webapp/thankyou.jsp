<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Registro realizado</title>
  </head>
  <body>
    <h3>Gracias por registrarte <s:property value="firstName" /></h3>            

    <p><a href="<s:url action='index' />" >Volver a la página de inicio</a>.</p>
  </body>
</html>

