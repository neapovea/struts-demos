<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>IFCT034PO | 08-Interceptors</title>
  </head>
  <body>
    <h1>¡Bienvenido a Struts 2!</h1>

    <p><a href="register.jsp">Registrate</a> para poder acceder a todas las funcionalidades.</p>
  </body>
</html>
