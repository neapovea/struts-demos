package es.training.struts.ajax.api;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.opensymphony.xwork2.Action;

import es.training.struts.ajax.action.MainAction;

public class UsersApi extends MainAction {
	
	private static final long serialVersionUID = 1L;
	
	private boolean status;
	
	private String label;
	
	private String userName;
	
	private File userFile;
	
	private String userFileContentType;
	
	private String userFileFileName;
	
	private String filePath;
	
	private String message;
	
	public boolean getStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public String getLabel() {
		return label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public void setUserFile(File userFile) {
		this.userFile = userFile;
	}
	
	public String getUserFileContentType() {
		return userFileContentType;
	}
	
	public void setUserFileContentType(String userFileContentType) {
		this.userFileContentType = userFileContentType;
	}
	
	public String getUserFileFileName() {
		return userFileFileName;
	}
	
	public void setUserFileFileName(String userFileFileName) {
		this.userFileFileName = userFileFileName;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public String saveUser() throws IOException {
		boolean processStatus = false;
		String statusLabel = "red";
		String responseMessage = "Image file uploading.";
		
		if(userFileFileName == null) {
			responseMessage = "Select your image file, and submit again.";
		} else {
		
			filePath = FILE_PATH;
		
			responseMessage = "Image file is saved at "+filePath;
			try {
				File destFile = new File(filePath, userFileFileName);
				FileUtils.copyFile(userFile, destFile);
				processStatus = true;
				statusLabel = "green";
				responseMessage = "Image file uploaded.";
			} catch (IOException e) {
				responseMessage = "Image file upload failed!";
				e.printStackTrace();
			}
		}
		
		setStatus(processStatus);
		setLabel(statusLabel);
		setMessage(responseMessage);
		return Action.SUCCESS;
	}
}
