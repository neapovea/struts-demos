package es.training.struts.ajax.action;

import java.io.File;
import java.util.ResourceBundle;

import com.opensymphony.xwork2.ActionSupport;

public class MainAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	public static String FILE_PATH = null;
	
	public MainAction() {
		
		ResourceBundle file = ResourceBundle.getBundle("file");
		
		FILE_PATH = file.getString("file.file-path");
		
		File directory = new File(FILE_PATH);
		
		if (!directory.exists()) {
		    try {
		    	directory.mkdir();
		    } catch(SecurityException se) {
		        se.printStackTrace();
		    }
		}
	}
}
