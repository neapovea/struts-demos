<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Struts2 Ajax File Upload Application</title>
<script src="${pageContext.request.contextPath}/js/jquery-1.10.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$("#userForm").on('submit', (function(e) {
			e.preventDefault();
			$.ajax({
				url : "api/saveUser",
				type : "POST",
				data : new FormData(this),
				contentType : false,
				cache : false,
				processData : false,
				success : function(data) {
					$("#messageBox").html(data.message);
					if(data.status == true) {
						$("#userSaveInfo").html(JSON.stringify(data));
					}
				},
				error : function() {
					
				}
			});
		}));
	});
</script>
</head>
<body>
	<h1>Struts2 Ajax File Upload Application</h1>
	<hr>
	<div id="messageBox"></div>
	<div>
	 	<h2> Save user information</h2>
		<form id="userForm" action="saveUser" method="post" enctype="multipart/form-data">
		<table border="1" style="border-collapse:collapse;" cellpadding="10">
			<tr>
				<td>Name</td>
				<td><input name="userName" type="text" class="inputFile" /><br></td>
			</tr>
			<tr>
				<td>Upload your image</td>
				<td><input name="userFile" type="file" class="inputFile" /><br></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Submit" class="btnSubmit" /></td>
			</tr>
		</table>
	</form>
	</div>
	<div id="userSaveInfo"></div>
</body>
</html>