<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>IFCT034PO | 05-ProcesingForms</title>
  </head>
  <body>
    <h1>¡Bienvenido a Struts 2!</h1>
    
	<!-- link -->
    <p><a href="<s:url action='hello'/>">Hola Mundo</a></p>
    
    <!-- link with parameters-->
    <s:url action="hello" var="helloLink">
      <s:param name="userName">Pedro Saez</s:param>
    </s:url>
    <p><a href="${helloLink}">Hola Pedro Saez</a></p>
    
    <!-- form -->
    <p>Obten un saludo personalizado rellenando y enviando el formulario.</p>
    <s:form action="hello">
      <s:textfield name="userName" label="Tu nombre" />
      <s:submit value="Enviar"/>
    </s:form>

    <p><a href="register.jsp">Registrate</a> para poder acceder a todas las funcionalidades.</p>
  </body>
</html>
