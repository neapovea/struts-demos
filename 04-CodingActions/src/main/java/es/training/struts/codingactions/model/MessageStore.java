package es.training.struts.codingactions.model;

public class MessageStore {
	
    private String message;
    
    public MessageStore() {
        setMessage("Hola usuario");
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
