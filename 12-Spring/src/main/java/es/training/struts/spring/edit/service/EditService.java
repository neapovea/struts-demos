package es.training.struts.spring.edit.service;

import es.training.struts.spring.edit.model.Person;

public interface EditService {

	Person getPerson();

	void savePerson(Person personBean);

}
