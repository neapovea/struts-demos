package es.training.struts.helloworld.model;

public class MessageStore {
    
    private String message;
    
    public MessageStore() {
        message = "Hola usuario";
    }

    public String getMessage() {
        return message;
    }

}
