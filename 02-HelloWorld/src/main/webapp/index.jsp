<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>IFCT034PO | 02-HelloWorld</title>
  </head>
  <body>
    <h1>¡Bienvenido a Struts 2!</h1>
    <p><a href="<s:url action='hello'/>">Hola Mundo</a></p>
  </body>
</html>
