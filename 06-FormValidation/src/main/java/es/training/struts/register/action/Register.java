package es.training.struts.register.action;

import com.opensymphony.xwork2.ActionSupport;
import es.training.struts.register.model.Person;


public class Register extends ActionSupport {
	
	private static final long serialVersionUID = 1L;
	
	private Person personBean;

	
	public String execute() throws Exception {
		//call Service class to store personBean's state in database
		return SUCCESS;
	}
	
	public void validate(){
		
		if ( personBean.getFirstName().length() == 0 ){	
			addFieldError( "personBean.firstName", "El nombre es obligatorio." );
		}
		
				
		if ( personBean.getEmail().length() == 0 ){	
			addFieldError( "personBean.email", "El email es obligatorio." );
		}
		
		if ( personBean.getAge() < 18 ){	
			addFieldError( "personBean.age", "Años es obligatorio y debe ser 18 o mayor" );
		}
	}
	
	public Person getPersonBean() {
		return personBean;
	}
	
	public void setPersonBean(Person person) {
		personBean = person;
	}

}
